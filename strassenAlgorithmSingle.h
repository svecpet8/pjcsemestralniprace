//
// Created by svecpe on 1.1.19.
//

#ifndef SEMESTRALKA_STRASSENALGORITHM_H
#define SEMESTRALKA_STRASSENALGORITHM_H

#include "matrix.h"

unique_ptr<matrix> strassenSingleThread(const unique_ptr<matrix> &matrixA, const unique_ptr<matrix> &matrixB);

unique_ptr<matrix>
strassenMultiplying(const unique_ptr<matrix> &matrixA, const unique_ptr<matrix> &matrixB, const int &numbersInRow);

#endif //SEMESTRALKA_STRASSENALGORITHM_H
