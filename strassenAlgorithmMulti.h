//
// Created by svecpe on 25.1.19.
//

#ifndef SEMESTRALKA_STRASSENALGORITHMMULTI_H
#define SEMESTRALKA_STRASSENALGORITHMMULTI_H

#include "matrix.h"
#include <future>
#include <mutex>
#include <memory>

unique_ptr<matrix> strassenMultiThread(const unique_ptr<matrix> &matrixA, const unique_ptr<matrix> &matrixB,int threads);

unique_ptr<matrix>
strassenMultiMultiplying(const unique_ptr<matrix> &matrixA, const unique_ptr<matrix> &matrixB, const int &numbersInRow);

void getFutures(unique_ptr<matrix> *pPtr[7], future<unique_ptr<matrix>> *pFuture[7], bool pBoolean[7]);

bool isThreadFree();

#endif //SEMESTRALKA_STRASSENALGORITHMMULTI_H
