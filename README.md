## Zadání:
    Násobení matic

## Implementace:
###   algoritmus:
    Použit Strassenův algoritmus. 
        Základní úpravy oproti definici algoritmu:
            Algoritmus je rekurzivní, rozkládani matic, dokud by nebyly degenerovany na čísla by bylo pomalé, takže jsem zvolil
            , aby pro velikost matice mensi, nez 64 se pouzilo klasické násobení matic podle definice. 
            Pro verzi s jedním vláknem pracuji s pomocnými maticemi, aby se jich tvořilo co nejméně.
            Pro verzi s více vlákny kontroluji, pokud můžu spustit nové vlákno, pokud ano, pomocí future spustím, na konci algoritmu spojím dílčí části.
        
###    soubory: (hpp i cpp)
        main : Obsluha reakcí na argumenty v příkazové řádce (neobsahuje hpp)
        Matrix : Funkce mimo třídu + třída, obsahuje funkce a metody týkající se práce s maticemi
        StrassenAlgorithmSingle : Implementace  Strassenova algoritmu (jedno vlákno)
        StrassenAlgorithmMulti :  Implementace  Strassenova algoritmu (více vláken)
   **více popis implementace viz komentáře v kodu.** <br />
<br />    
## výsledky běhu programu:
pro rozměry matic **1024x1024**   <br />

| implementace  |  průměrné naměřené časy (sec) |
| :-------------: | :-------------: |
|  Naivní   | 172  |
| JednoVlákno   | 107  |
| VíceVláken   | 29  |

pro rozměry matic **512x512**   <br />

| implementace  |  průměrné naměřené časy (sec) |
| :-------------: | :-------------: |
|  Naivní   | 20,3  |
| JednoVlákno   | 15,1  |
| VíceVláken   | 4,1  |
        
## prostředí: <br />
Lenovo IdeaPad Z50-75 (technicke detaily viz. https://www.czc.cz/lenovo-ideapad-z50-75-cerna_3/161697/produkt )<br />
    Procesor:  AMD A10-7300 Radeon R6, 4jádra/4vlákna<br />
operační systém: Ubuntu 16.04
        
        