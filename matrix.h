//
// Created by svecpe on 9.1.19.
//

#ifndef SEMESTRALKA_MATRIX_H
#define SEMESTRALKA_MATRIX_H

#include <string>
#include <sstream>
#include <vector>
#include "matrix.h"
#include <memory>

using namespace std;
using matrixType =  vector<vector<int> >;
using matrixRow =  vector<int>;

class matrix {
public:
    static const int StrassenminInRow = 64;

    //methods
    void matrixToFile(const string &fileName);

    void addRow(std::vector<std::string> numbers, int numbersInRow, int actualRow);

    static std::unique_ptr<matrix> loadMatrix(const string &fileName, const int numbersInRow);

    void intializingZeroMatrix(const int numbersInRow);

    static std::unique_ptr<matrix>
    basicMatrixMultiplication(const std::unique_ptr<matrix> &matrixA, const std::unique_ptr<matrix> &matrixB,
                              const int &numbersInRow);

    int getNumbersInRow() const;

    // special members
    matrix(const int numbersInRow);

    matrix(const matrix &other) = default;

    matrix &operator=(const matrix &other) = default;

    matrix(matrix &&other) = default;

    matrix &operator=(matrix &&other) = default;

    ~matrix() = default;

    //operators
    matrixRow &operator[](int index) {
        return matrixContent[index];
    }

    const matrixRow &operator[](const int index) const {
        return matrixContent[index];
    }

    bool operator==(const matrix &rhs) const;

    bool operator!=(const matrix &rhs) const;

private:
    int numbersInRow;
    matrixType matrixContent;
};

int getNumbersInLine(const string &fileName);

unique_ptr<matrix> createSubMatrix(const unique_ptr<matrix> &a, const int newMatrixSize, const int rangeA[]);

unique_ptr<matrix> matrixesSum(const unique_ptr<matrix> &a, const unique_ptr<matrix> &b, const int newMatrixSize);

unique_ptr<matrix> matrixesSub(const unique_ptr<matrix> &a, const unique_ptr<matrix> &b, const int newMatrixSize);

void matrixesSum(const unique_ptr<matrix> &a, const unique_ptr<matrix> &b, const int newMatrixSize,
                 unique_ptr<matrix> &result);

void matrixesSub(const unique_ptr<matrix> &a, const unique_ptr<matrix> &b, const int newMatrixSize,
                 unique_ptr<matrix> &result);
void matrixesJoin(unique_ptr<matrix> &result, const unique_ptr<matrix> &C11,const unique_ptr<matrix> &C12,const unique_ptr<matrix> &C21,const unique_ptr<matrix> &C22, const int &subMatrixesSize);



#endif //SEMESTRALKA_MATRIX_H
