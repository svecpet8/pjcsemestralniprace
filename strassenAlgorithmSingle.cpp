//
// Created by svecpe on 1.1.19.
//
#include "strassenAlgorithmSingle.h"

unique_ptr<matrix> strassenSingleThread(const unique_ptr<matrix> &matrixA, const unique_ptr<matrix> &matrixB) {
    return move(strassenMultiplying(matrixA, matrixB, matrixA->getNumbersInRow()));
}

// todo: zkusit s vytvorenim resultem, viz. textak
unique_ptr<matrix>
strassenMultiplying(const unique_ptr<matrix> &matrixA, const unique_ptr<matrix> &matrixB, const int &numbersInRow) {
// create result matrix
    unique_ptr<matrix> result = make_unique<matrix>(numbersInRow);
    int subMatrixesSize = numbersInRow / 2;

// end of recursion
// classic multiplying for matrixes < than 64 numbers in row
    if (numbersInRow <= matrix::StrassenminInRow) {
        result = matrix::basicMatrixMultiplication(matrixA, matrixB, numbersInRow);
        return move(result);
    }

// set submatrixesRange;
    int matrix11[2];
    matrix11[0] = 0;
    matrix11[1] = 0;
    int matrix12[2];
    matrix12[0] = 0;
    matrix12[1] = subMatrixesSize;
    int matrix21[2];
    matrix21[0] = subMatrixesSize;
    matrix21[1] = 0;
    int matrix22[2];
    matrix22[0] = subMatrixesSize;
    matrix22[1] = subMatrixesSize;

// set submatrixes
    unique_ptr<matrix> A11 = createSubMatrix(matrixA, subMatrixesSize, matrix11);
    unique_ptr<matrix> A12 = createSubMatrix(matrixA, subMatrixesSize, matrix12);
    unique_ptr<matrix> A21 = createSubMatrix(matrixA, subMatrixesSize, matrix21);
    unique_ptr<matrix> A22 = createSubMatrix(matrixA, subMatrixesSize, matrix22);

    unique_ptr<matrix> B11 = createSubMatrix(matrixB, subMatrixesSize, matrix11);
    unique_ptr<matrix> B12 = createSubMatrix(matrixB, subMatrixesSize, matrix12);
    unique_ptr<matrix> B21 = createSubMatrix(matrixB, subMatrixesSize, matrix21);
    unique_ptr<matrix> B22 = createSubMatrix(matrixB, subMatrixesSize, matrix22);

    unique_ptr<matrix> tmpResult = make_unique<matrix>(subMatrixesSize);
    unique_ptr<matrix> tmpResult2 = make_unique<matrix>(subMatrixesSize);

/*
   M1 = (A11 + A22)(B11 + B22)
*/

    matrixesSum(A11, A22, subMatrixesSize,tmpResult);
    matrixesSum(B11, B22, subMatrixesSize, tmpResult2);
    unique_ptr<matrix> M1 = strassenMultiplying(tmpResult, tmpResult2, subMatrixesSize);
/*
    M2 = (A21 + A22) B11
*/

    matrixesSum(A21, A22, subMatrixesSize,tmpResult);
    unique_ptr<matrix> M2 = strassenMultiplying(tmpResult, B11, subMatrixesSize);
/*
          M3 = A11 (B12 - B22)
*/

    matrixesSub(B12, B22, subMatrixesSize, tmpResult);
    unique_ptr<matrix> M3 = strassenMultiplying(A11, tmpResult, subMatrixesSize);

/*
          M4 = A22 (B21 - B11)
*/
    matrixesSub(B21, B11, subMatrixesSize,tmpResult);
    unique_ptr<matrix> M4 = strassenMultiplying(A22, tmpResult, subMatrixesSize);

/*
              M5 = (A11 + A12) B22
*/
    matrixesSum(A11, A12, subMatrixesSize,tmpResult);
    unique_ptr<matrix> M5 = strassenMultiplying(tmpResult, B22, subMatrixesSize);

/*
              M6 = (A21 - A11) (B11 + B12)
*/
    matrixesSub(A21, A11, subMatrixesSize,tmpResult);
    matrixesSum(B11, B12, subMatrixesSize, tmpResult2);
    unique_ptr<matrix> M6 = strassenMultiplying(tmpResult, tmpResult2, subMatrixesSize);

/*
              M7 = (A12 - A22) (B21 + B22)
*/
    matrixesSub(A12, A22, subMatrixesSize, tmpResult);
    matrixesSum(B21, B22, subMatrixesSize, tmpResult2);
    unique_ptr<matrix> M7 = strassenMultiplying(tmpResult, tmpResult2, subMatrixesSize);

/*
              C11 = M1 + M4 - M5 + M7
*/
    unique_ptr<matrix> c11FirstHalf = matrixesSub(matrixesSum(M1, M4, subMatrixesSize), M5, subMatrixesSize);
    unique_ptr<matrix> C11 = matrixesSum(c11FirstHalf, M7, subMatrixesSize);
/*
    C12 = M3 + M5
*/
    unique_ptr<matrix> C12 = matrixesSum(M3, M5, subMatrixesSize);
/*
    C21 = M2 + M4
*/
    unique_ptr<matrix> C21 = matrixesSum(M2, M4, subMatrixesSize);
/*
    C22 = M1 - M2 + M3 + M6
*/
    unique_ptr<matrix> c22FirstHalf = matrixesSum(matrixesSub(M1, M2, subMatrixesSize), M3, subMatrixesSize);
    unique_ptr<matrix> C22 = matrixesSum(c22FirstHalf, M6, subMatrixesSize);
/*
    from C11 to C22 join to one matrix
*/
    matrixesJoin(result,C11,C12,C21,C22,subMatrixesSize);
    return move(result);
}