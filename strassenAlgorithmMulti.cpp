//
// Created by svecpe on 25.1.19.
//
#include "strassenAlgorithmMulti.h"
#include <iostream>

int MAXTHREADS;
int counter = 0;
std::mutex cntMutex;

unique_ptr<matrix>
strassenMultiThread(const unique_ptr<matrix> &matrixA, const unique_ptr<matrix> &matrixB,int cores) {
    MAXTHREADS = cores*4;
    return move(strassenMultiMultiplying(matrixA, matrixB, matrixA->getNumbersInRow()));
}

unique_ptr<matrix>
strassenMultiMultiplying(const unique_ptr<matrix> &matrixA, const unique_ptr<matrix> &matrixB,
                         const int &numbersInRow) {
// create result matrix
    unique_ptr<matrix> result = make_unique<matrix>(numbersInRow);
    int subMatrixesSize = numbersInRow / 2;
// end of recursion
    if (numbersInRow <= matrix::StrassenminInRow) {
        result = matrix::basicMatrixMultiplication(matrixA, matrixB, numbersInRow);
        return move(result);
    }
// prepare futures
    bool futureInThreads[7] = {false, false, false, false, false, false, false};

    std::future<unique_ptr<matrix>> futureM1;
    std::future<unique_ptr<matrix>> futureM2;
    std::future<unique_ptr<matrix>> futureM3;
    std::future<unique_ptr<matrix>> futureM4;
    std::future<unique_ptr<matrix>> futureM5;
    std::future<unique_ptr<matrix>> futureM6;
    std::future<unique_ptr<matrix>> futureM7;
    std::future<unique_ptr<matrix>> *futures[] = {&futureM1, &futureM2, &futureM3, &futureM4, &futureM5, &futureM6,
                                                  &futureM7};

// prepare M"s
    unique_ptr<matrix> M1;
    unique_ptr<matrix> M2;
    unique_ptr<matrix> M3;
    unique_ptr<matrix> M4;
    unique_ptr<matrix> M5;
    unique_ptr<matrix> M6;
    unique_ptr<matrix> M7;
    unique_ptr<matrix> *Ms[] = {&M1, &M2, &M3, &M4, &M5, &M6, &M7};

// set submatrixesRange;
    int matrix11[2];
    matrix11[0] = 0;
    matrix11[1] = 0;
    int matrix12[2];
    matrix12[0] = 0;
    matrix12[1] = subMatrixesSize;
    int matrix21[2];
    matrix21[0] = subMatrixesSize;
    matrix21[1] = 0;
    int matrix22[2];
    matrix22[0] = subMatrixesSize;
    matrix22[1] = subMatrixesSize;

// set submatrixes
    unique_ptr<matrix> A11 = createSubMatrix(matrixA, subMatrixesSize, matrix11);
    unique_ptr<matrix> A12 = createSubMatrix(matrixA, subMatrixesSize, matrix12);
    unique_ptr<matrix> A21 = createSubMatrix(matrixA, subMatrixesSize, matrix21);
    unique_ptr<matrix> A22 = createSubMatrix(matrixA, subMatrixesSize, matrix22);

    unique_ptr<matrix> B11 = createSubMatrix(matrixB, subMatrixesSize, matrix11);
    unique_ptr<matrix> B12 = createSubMatrix(matrixB, subMatrixesSize, matrix12);
    unique_ptr<matrix> B21 = createSubMatrix(matrixB, subMatrixesSize, matrix21);
    unique_ptr<matrix> B22 = createSubMatrix(matrixB, subMatrixesSize, matrix22);

/*
   M1 = (A11 + A22)(B11 + B22)
*/

    if (isThreadFree()) {
        futureInThreads[0] = true;
        futureM1 = std::async(std::launch::async, [&A11, &A22, &B11, &B22, &subMatrixesSize] {
            unique_ptr<matrix> m1FirstHalf = matrixesSum(A11, A22, subMatrixesSize);
            unique_ptr<matrix> m1SecondHalf = matrixesSum(B11, B22, subMatrixesSize);
            unique_ptr<matrix> M1 = strassenMultiMultiplying(m1FirstHalf, m1SecondHalf, subMatrixesSize);
            return M1;
        });
    } else {
        unique_ptr<matrix> m1FirstHalf = matrixesSum(A11, A22, subMatrixesSize);
        unique_ptr<matrix> m1SecondHalf = matrixesSum(B11, B22, subMatrixesSize);
        M1 = strassenMultiMultiplying(m1FirstHalf, m1SecondHalf, subMatrixesSize);
    }

/*
    M2 = (A21 + A22) B11
*/

    if (isThreadFree()) {
        futureInThreads[1] = true;
        futureM2 = std::async(std::launch::async, [&A21, &A22, &B11, &subMatrixesSize] {
            unique_ptr<matrix> m2FirstHalf = matrixesSum(A21, A22, subMatrixesSize);
            unique_ptr<matrix> M2 = strassenMultiMultiplying(m2FirstHalf, B11, subMatrixesSize);
            return M2;
        });
    } else {
        unique_ptr<matrix> m2FirstHalf = matrixesSum(A21, A22, subMatrixesSize);
        M2 = strassenMultiMultiplying(m2FirstHalf, B11, subMatrixesSize);
    }

/*
          M3 = A11 (B12 - B22)
*/

    if (isThreadFree()) {
        futureInThreads[2] = true;
        futureM3 = std::async(std::launch::async, [&A11, &B12, &B22, &subMatrixesSize] {
            unique_ptr<matrix> m3SecondHalf = matrixesSub(B12, B22, subMatrixesSize);
            unique_ptr<matrix> M3 = strassenMultiMultiplying(A11, m3SecondHalf, subMatrixesSize);
            return M3;
        });
    } else {
        unique_ptr<matrix> m3SecondHalf = matrixesSub(B12, B22, subMatrixesSize);
        M3 = strassenMultiMultiplying(A11, m3SecondHalf, subMatrixesSize);
    }

/*
          M4 = A22 (B21 - B11)
*/

    if (isThreadFree()) {
        futureInThreads[3] = true;
        futureM4 = std::async(std::launch::async, [&A22, &B11, &B21, &subMatrixesSize] {
            unique_ptr<matrix> m4SecondHalf = matrixesSub(B21, B11, subMatrixesSize);
            unique_ptr<matrix> M4 = strassenMultiMultiplying(A22, m4SecondHalf, subMatrixesSize);
            return M4;
        });
    } else {
        unique_ptr<matrix> m4SecondHalf = matrixesSub(B21, B11, subMatrixesSize);
        M4 = strassenMultiMultiplying(A22, m4SecondHalf, subMatrixesSize);
    }

/*
              M5 = (A11 + A12) B22
*/

    if (isThreadFree()) {
        futureInThreads[4] = true;
        futureM5 = std::async(std::launch::async, [&A11, &A12, &B22, &subMatrixesSize] {
            unique_ptr<matrix> m5FirstHalf = matrixesSum(A11, A12, subMatrixesSize);
            unique_ptr<matrix> M5 = strassenMultiMultiplying(m5FirstHalf, B22, subMatrixesSize);
            return M5;
        });
    } else {
        unique_ptr<matrix> m5FirstHalf = matrixesSum(A11, A12, subMatrixesSize);
        M5 = strassenMultiMultiplying(m5FirstHalf, B22, subMatrixesSize);
    }

/*
              M6 = (A21 - A11) (B11 + B12)
*/

    if (isThreadFree()) {
        futureInThreads[5] = true;
        futureM6 = std::async(std::launch::async, [&A11, &A21, &B11, &B12, &subMatrixesSize] {
            unique_ptr<matrix> m6FirstHalf = matrixesSub(A21, A11, subMatrixesSize);
            unique_ptr<matrix> m6SecondHalf = matrixesSum(B11, B12, subMatrixesSize);
            unique_ptr<matrix> M6 = strassenMultiMultiplying(m6FirstHalf, m6SecondHalf, subMatrixesSize);
            return M6;
        });
    } else {
        unique_ptr<matrix> m6FirstHalf = matrixesSub(A21, A11, subMatrixesSize);
        unique_ptr<matrix> m6SecondHalf = matrixesSum(B11, B12, subMatrixesSize);
        M6 = strassenMultiMultiplying(m6FirstHalf, m6SecondHalf, subMatrixesSize);
    }

/*
              M7 = (A12 - A22) (B21 + B22)
*/

    if (isThreadFree()) {
        futureInThreads[6] = true;
        futureM7 = std::async(std::launch::async, [&A12, &A22, &B21, &B22, &subMatrixesSize] {
            unique_ptr<matrix> m7FirstHalf = matrixesSub(A12, A22, subMatrixesSize);
            unique_ptr<matrix> m7SecondHalf = matrixesSum(B21, B22, subMatrixesSize);
            unique_ptr<matrix> M7 = strassenMultiMultiplying(m7FirstHalf, m7SecondHalf, subMatrixesSize);
            return M7;
        });
    } else {
        unique_ptr<matrix> m7FirstHalf = matrixesSub(A12, A22, subMatrixesSize);
        unique_ptr<matrix> m7SecondHalf = matrixesSum(B21, B22, subMatrixesSize);
        M7 = strassenMultiMultiplying(m7FirstHalf, m7SecondHalf, subMatrixesSize);
    }

    getFutures(Ms, futures, futureInThreads);

/*
              C11 = M1 + M4 - M5 + M7
*/

    unique_ptr<matrix> c11FirstHalf = matrixesSub(matrixesSum(M1, M4, subMatrixesSize), M5, subMatrixesSize);
    unique_ptr<matrix> C11 = matrixesSum(c11FirstHalf, M7, subMatrixesSize);

/*
    C12 = M3 + M5
*/

    unique_ptr<matrix> C12 = matrixesSum(M3, M5, subMatrixesSize);

/*
    C21 = M2 + M4
*/

    unique_ptr<matrix> C21 = matrixesSum(M2, M4, subMatrixesSize);

/*
    C22 = M1 - M2 + M3 + M6
*/

    unique_ptr<matrix> c22FirstHalf = matrixesSum(matrixesSub(M1, M2, subMatrixesSize), M3, subMatrixesSize);
    unique_ptr<matrix> C22 = matrixesSum(c22FirstHalf, M6, subMatrixesSize);

/*
    from C11 to C22 join to one matrix
*/
    matrixesJoin(result,C11,C12,C21,C22,subMatrixesSize);
    return move(result);
}

// if M1-M7 was calculated by using future, function set value to correct M.
void getFutures(unique_ptr<matrix> *pMs[7], future<unique_ptr<matrix>> *pFuturesM[7], bool pInThreads[7]) {
    for (int i = 0; i < 7; i++) {
        if (pInThreads[i]) {
            *pMs[i] = (pFuturesM)[i]->get();
            std::unique_lock<std::mutex> lock(cntMutex);
            counter--;
            lock.unlock();
        }
    }
}

// return false, if program is using max number of threads
bool isThreadFree() {
    std::unique_lock<std::mutex> lock(cntMutex);
    if (counter < MAXTHREADS) {
        counter++;
        return true;
    }
    return false;
}
