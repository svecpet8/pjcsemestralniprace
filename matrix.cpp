//
// Created by svecpe on 9.1.19.
//

#include <random>
#include <fstream>
#include <iostream>
#include <thread>
#include <vector>
#include <chrono>
#include <fstream>
#include <string>
#include <sstream>
#include "matrix.h"

using namespace std;

void matrix::matrixToFile(const string &fileName) {
    ofstream myfile;
    myfile.open(fileName);
    for (int i = 0; i < numbersInRow; i++) {
        for (int j = 0; j < numbersInRow; j++) {
            myfile << matrixContent[i][j] << " ";
        }
        myfile << endl;
    }
    myfile.close();
}

void matrix::addRow(std::vector<std::string> numbers, int numbersInRow, int actualRow) {
    int i = 0;
    matrixRow tmp(numbersInRow);
    for (string &numberStr : numbers) {
        tmp[i] = stod(numberStr);
        i++;
    }
    for (; i < numbersInRow; i++) {
        tmp[i] = 0;
    }
    matrixContent[actualRow] = move(tmp);
}

template<class Container>
void splitString(const std::string &str, Container &cont, char delim = ' ') {
    std::stringstream ss(str);
    std::string token;
    while (std::getline(ss, token, delim)) {
        cont.push_back(token);
    }
}

int getNumbersInLine(const string &fileName) {
    string line;
    std::vector<std::string> numbers;
    ifstream infile;
    infile.open(fileName);
    getline(infile, line);
    splitString(line, numbers, ' ');
    return numbers.size();
}

// load matrix (if matrix is smaller than is expected by numbersInRow add 0 )
unique_ptr<matrix> matrix::loadMatrix(const string &fileName, const int numbersInRow) {
    ifstream infile;
    infile.open(fileName);
    if (infile.fail()) {
        throw std::invalid_argument("wrong argument filename: " + fileName + " does not exist");
    }
    unique_ptr<matrix> loadMatrix = make_unique<matrix>(numbersInRow);
    for (int i = 0; i < numbersInRow; i++) {
        string line;
        std::vector<std::string> numbers;
        getline(infile, line);
        splitString(line, numbers, ' ');
        loadMatrix->addRow(numbers, numbersInRow, i);
    }
    infile.close();
    return move(loadMatrix);
}

void matrix::intializingZeroMatrix(const int numbersInRow) {
    for (int i = 0; i < numbersInRow; i++) {
        matrixRow tmp(numbersInRow);
        for (int j = 0; j < numbersInRow; j++) {
            tmp[j] = 0;
        }
        matrixContent[i] = std::move(tmp);
    }
}

unique_ptr<matrix>
matrix::basicMatrixMultiplication(const unique_ptr<matrix> &matrixA, const unique_ptr<matrix> &matrixB,
                                  const int &numbersInRow) {
    unique_ptr<matrix> resultMat = make_unique<matrix>(numbersInRow);
    for (int i = 0; i < numbersInRow; i++) {
        for (int j = 0; j < numbersInRow; j++) {
            for (int k = 0; k < numbersInRow; k++) {
                (*resultMat)[i][j] += (*matrixA)[i][k] * (*matrixB)[k][j];
            }
        }
    }
    return move(resultMat);
}

matrix::matrix(const int numbersInRow) : numbersInRow(numbersInRow),
                                         matrixContent(numbersInRow, matrixRow(numbersInRow, 0)) {
}

int matrix::getNumbersInRow() const {
    return numbersInRow;
}

bool matrix::operator==(const matrix &rhs) const {
    return numbersInRow == rhs.numbersInRow &&
           matrixContent == rhs.matrixContent;
}

bool matrix::operator!=(const matrix &rhs) const {
    return !(rhs == *this);
}

void matrixesSum(const unique_ptr<matrix> &a, const unique_ptr<matrix> &b, const int newMatrixSize,
                 unique_ptr<matrix> &result) {
    //unique_ptr<matrix> resultMat = make_unique<matrix>(newMatrixSize);
    for (int i = 0; i < newMatrixSize; i++) {
        for (int j = 0; j < newMatrixSize; j++) {
            (*result)[i][j] = (*a)[i][j] + (*b)[i][j];
        }
    }
}

unique_ptr<matrix> matrixesSum(const unique_ptr<matrix> &a, const unique_ptr<matrix> &b, const int newMatrixSize) {
    unique_ptr<matrix> resultMat = make_unique<matrix>(newMatrixSize);
    for (int i = 0; i < newMatrixSize; i++) {
        for (int j = 0; j < newMatrixSize; j++) {
            (*resultMat)[i][j] = (*a)[i][j] + (*b)[i][j];
        }
    }
    return move(resultMat);
}

unique_ptr<matrix> createSubMatrix(const unique_ptr<matrix> &a, const int newMatrixSize, const int rangeA[]) {
    unique_ptr<matrix> resultMat = make_unique<matrix>(newMatrixSize);
    for (int i = 0; i < newMatrixSize; i++) {
        for (int j = 0; j < newMatrixSize; j++) {
            (*resultMat)[i][j] = (*a)[i + rangeA[0]][j + rangeA[1]];
        }
    }
    return move(resultMat);
}

void matrixesSub(const unique_ptr<matrix> &a, const unique_ptr<matrix> &b, const int newMatrixSize,
                 unique_ptr<matrix> &result) {
    for (int i = 0; i < newMatrixSize; i++) {
        for (int j = 0; j < newMatrixSize; j++) {
            (*result)[i][j] = (*a)[i][j] - (*b)[i][j];
        }
    }
}

unique_ptr<matrix> matrixesSub(const unique_ptr<matrix> &a, const unique_ptr<matrix> &b, const int newMatrixSize) {
    unique_ptr<matrix> resultMat = make_unique<matrix>(newMatrixSize);
    for (int i = 0; i < newMatrixSize; i++) {
        for (int j = 0; j < newMatrixSize; j++) {
            (*resultMat)[i][j] = (*a)[i][j] - (*b)[i][j];
        }
    }
    return move(resultMat);
}
void matrixesJoin(unique_ptr<matrix> &result, const unique_ptr<matrix> &C11,const unique_ptr<matrix> &C12,const unique_ptr<matrix> &C21,const unique_ptr<matrix> &C22, const int &subMatrixesSize){
    for (int i = 0; i < subMatrixesSize; i++) {
        for (int j = 0; j < subMatrixesSize; j++) {
            (*result)[i][j] = (*C11)[i][j];
            (*result)[i][j + subMatrixesSize] = (*C12)[i][j];
            (*result)[i + subMatrixesSize][j] = (*C21)[i][j];
            (*result)[i + subMatrixesSize][j + subMatrixesSize] = (*C22)[i][j];
        }
    }
}
