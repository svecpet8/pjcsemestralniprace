#include <iostream>
#include <thread>
#include <vector>
#include <random>
#include <chrono>
#include <fstream>
#include <string>
#include <sstream>
#include "matrix.h"
#include "strassenAlgorithmSingle.h"
#include "strassenAlgorithmMulti.h"

using namespace std;


// tmp functions

enum class MultiplyingType {
    BASIC, SINGLE, MULTI
};
namespace timer {
    auto now() {
        return std::chrono::high_resolution_clock::now();
    }

    template<typename TimePoint>
    long long to_ms(TimePoint tp) {
        return std::chrono::duration_cast<std::chrono::milliseconds>(tp).count();
    }

}

int getNextPow2(int number) {
    return pow(2, ceil(log(number) / log(2)));
}

vector <string> split(string const &line, char delim) {
    vector <string> res;
    if (line.empty())
        return res;
    size_t first = 0;
    size_t last;
    while ((last = line.find(delim, first)) != string::npos) {
        res.push_back(line.substr(first, last - first));
        first = last + 1;
    }
    res.push_back(line.substr(first));
    return res;
}

// function to findOut if vector contains concrete MultiplyingType
bool typesContains(vector < MultiplyingType >
const &types,
MultiplyingType const &concreteType
){
for (
auto type
: types) {
if (type==concreteType){
return true;
}
}
return false;
}

void generate_RandomMatrix(const string &fileName, const int &numbersInRow) {
    ofstream myfile;
    myfile.open(fileName);
    for (int i = 0; i < numbersInRow; i++) {
        for (int j = 0; j < numbersInRow; j++) {
            myfile << (rand() % 100) << " ";
        }
        myfile << endl;
    }
    myfile.close();
}

// select which multiplication is choosen and will be started
vector <MultiplyingType> selectedMultiplying(string const &str) {
    vector <MultiplyingType> types;
    auto args = split(str, ',');
    if (args[0] == "All") {
        types.push_back(MultiplyingType::BASIC);
        types.push_back(MultiplyingType::SINGLE);
        types.push_back(MultiplyingType::MULTI);
        return types;
    }
    for (auto cmd : args) {
        if (cmd == "Basic") {
            types.push_back(MultiplyingType::BASIC);
        } else if (cmd == "Single") {
            types.push_back(MultiplyingType::SINGLE);
        } else if (cmd == "Multi") {
            types.push_back(MultiplyingType::MULTI);
        }
    }
    return types;
}

// funtion from input load (or generate) matrixes
void
selectedMatrixes(string const &str, unique_ptr <matrix> &matrixA, unique_ptr <matrix> &matrixB, int &numbersInRow) {
    string usingFileA;
    string usingFileB;
    auto args = split(str, ',');
    if (args.size() == 1) {
        if (args[0].find("rows=") != std::string::npos) {
            int numbersInRow = std::stoi(split(str, '=')[1]);
            usingFileA = "matrixA.txt";
            usingFileB = "matrixB.txt";
            generate_RandomMatrix(usingFileA, numbersInRow);
            generate_RandomMatrix(usingFileB, numbersInRow);
        } else {
            usingFileA = args[0];
            usingFileB = args[0];
        }
    } else if (args.size() == 2) {
        usingFileA = args[0];
        usingFileB = args[1];
    } else {
        throw invalid_argument("Wrong number of arguments. (Matrixes)");
    }
    // get next power of 2 from bigger matrix
    numbersInRow = getNextPow2(std::max(getNumbersInLine(usingFileA), getNumbersInLine(usingFileB)));
    // extend the matrix to findout numbersInRow
    matrixA = matrix::loadMatrix(usingFileA, numbersInRow);
    matrixB = matrix::loadMatrix(usingFileB, numbersInRow);
}

void printManual() {
    cout << "formát zadání zprávy je následující:" << endl;
    cout << "Zvolím násobení, které chci používat, pokud více, oddělím čárkou. Poté následuje mezera a mám 3 možnosti:"
         << endl;
    cout << "1) Vyberu 2 soubory, kde mám již matice vytvořené" << endl;
    cout << "př.    Basic,Single,Multi matrixA.txt,matrixB.txt" << endl;
    cout << "2) Vyberu 1 soubor, kde mám matici (vynásobí se sama se sebou)" << endl;
    cout << "př.    Single,Multi matrixA.txt" << endl;
    cout
            << "3) Do argumentu napíšu rows=číslo, vygeneruje se náhodná čtvercová matice o rozměrech danných zadaným číslem"
            << endl;
    cout << "př.    Single,Multi rows=1024" << endl;
    cout << "Po zadání podle vzorů se vypočítají matice, uloží do příslušných souborů a vypíší časy výpočtů." << endl
         << endl;
    cout
            << "Pro kontrolu toho, že se výpočty shodují: (musíte samozřejmě předtím výpočet různými způsoby na stejných maticích)"
            << endl;
    cout << "př.    compare matrixSingleResult.txt,matrixMultiResult.txt" << endl;
}

bool compareFiles(string const &str) {
    auto args = split(str, ',');
    string fileNameA = args[0];
    string fileNameB = args[1];
    unique_ptr <matrix> matrixA = matrix::loadMatrix(fileNameA, getNumbersInLine(fileNameA));
    unique_ptr <matrix> matrixB = matrix::loadMatrix(fileNameB, getNumbersInLine(fileNameB));
    if ((*matrixA) == (*matrixB)) {
        return true;
    }
    return false;
}

//  Požadavky : Jmeno souboru nesmi obsahovat "rows="
int main(int argc, char *argv[]) {
    string fileBasicMultiplication = "matrixBasicMultResult.txt";
    string fileSingleThreadMultiplication = "matrixSingleResult.txt";
    string fileMultiThreadMultiplication = "matrixMultiResult.txt";
    unique_ptr <matrix> matrixA;
    unique_ptr <matrix> matrixB;
    int numbersInRow;
    unsigned int cores = std::thread::hardware_concurrency();
    try {
        if (argc > 3 || argc < 1) {
            throw invalid_argument("Start of program contains wrong number of arguments.");
        }

        // help section
        string argument1 = argv[1];
        if (argument1.find("--help") != std::string::npos) {
            printManual();
            return 0;
        }
        if (argc != 3) {
            throw invalid_argument("Start of program contains wrong number of arguments.");
        }

        string argument2 = argv[2];

        // compare section
        if (argument1.find("compare") != std::string::npos) {
            if (compareFiles(argument2)) {
                cout << "Výsledky se rovnají!!! :)" << endl;
            } else {
                cout << "Výsledky se nerovnají! :(" << endl;
            }
            return 0;
        }

        vector <MultiplyingType> types = selectedMultiplying(argument1);
        selectedMatrixes(argument2, matrixA, matrixB, numbersInRow);
        if (typesContains(types, MultiplyingType::BASIC)) {
            // run basic multiplying
            auto t3 = timer::now();
            unique_ptr <matrix> basicMultResult = matrix::basicMatrixMultiplication(matrixA, matrixB,
                                                                                    numbersInRow);
            auto t4 = timer::now();
            basicMultResult->matrixToFile(fileBasicMultiplication);
            std::cout << "Basic multiplication result in file: " + fileBasicMultiplication << '\n';
            std::cout << "Basic multiplication time needed: " << timer::to_ms(t4 - t3) << '\n';
        }
        if (typesContains(types, MultiplyingType::SINGLE)) {
            // run single thread
            auto t1 = timer::now();
            unique_ptr <matrix> resultSingle = strassenSingleThread(matrixA, matrixB);
            auto t2 = timer::now();
            resultSingle->matrixToFile(fileSingleThreadMultiplication);
            std::cout << "Single thread result in file: " + fileSingleThreadMultiplication << '\n';
            std::cout << "Single thread time needed: " << timer::to_ms(t2 - t1) << '\n';
        }
        if (typesContains(types, MultiplyingType::MULTI)) {
            // run multi thread
            auto t5 = timer::now();
            unique_ptr <matrix> resultMulti = strassenMultiThread(matrixA, matrixB,cores);
            auto t6 = timer::now();
            resultMulti->matrixToFile(fileMultiThreadMultiplication);
            std::cout << "Multi threads result in file: " + fileMultiThreadMultiplication << '\n';
            std::cout << "Multi threads time needed: " << timer::to_ms(t6 - t5) << '\n';
        }
        return 0;
    }
    catch (std::exception &ex) {
        std::cout << "Exception: "
                  << ex.what() << "!\n";
    }
}

